﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BehaviorsExercise
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            OpacityZero();

        }
        void OnBtnClicked_ToSubmit(object sender, EventArgs e)
        {
            try
            {
                if ((EmailEntry.Text.Length != 0) || (PasswordEntry.Text.Length != 0) || (ConfirmPasswordEntry.Text.Length != 0))
                    IsCheckPassword();
                else
                    IsEmpty();

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            finally
            {
                OpacityZero();
                IsEmpty();

            }

        }
        void IsCheckPassword()
        {
            string emailEntry = EmailEntry.Text.Trim();
            string passwordEntry = PasswordEntry.Text.Trim();
            string confirmPasswordEntry = ConfirmPasswordEntry.Text.Trim();

            if (!PasswordEntry.Text.Equals(ConfirmPasswordEntry.Text) && (emailEntry.Length!=0 && passwordEntry.Length!=0 && confirmPasswordEntry.Length!=0))
                DisplayAlert("Alert", "Password not same", "Try again");
            else if(PasswordEntry.Text.Equals(ConfirmPasswordEntry.Text) && emailEntry.Length !=0 && passwordEntry.Length != 0 && confirmPasswordEntry.Length!=0)
                DisplayAlert("Success", "Perfect", "Okay");
            
        }
        void IsEmpty()
        {
            if ((EmailEntry.Text == null) || (PasswordEntry.Text == null) || (ConfirmPasswordEntry.Text == null) || (EmailEntry.Text.Length == 0) || (PasswordEntry.Text.Length == 0) || (ConfirmPasswordEntry.Text.Length == 0))
            {
                EmailEntry.Placeholder = "Required";
                EmailEntry.PlaceholderColor = Color.Red;
                PasswordEntry.Placeholder = "Required";
                PasswordEntry.PlaceholderColor = Color.Red;
                ConfirmPasswordEntry.Placeholder = "Required";
                ConfirmPasswordEntry.PlaceholderColor = Color.Red;

            }
        }
        void OpacityZero()
        {
            EmailLabel.Opacity = 0;
            PasswordLabel.Opacity = 0;
            ConfirmPasswordLabel.Opacity = 0;

        }
        void EmailEntry_Focused()
        {
            PasswordLabel.Opacity = 0;
            ConfirmPasswordLabel.Opacity = 0;
            EmailLabel.FadeTo(1, 250);

        }
        void PasswordEntry_Focused()
        {
            EmailLabel.Opacity = 0;
            ConfirmPasswordLabel.Opacity = 0;
            PasswordLabel.FadeTo(1, 250);

        }
        void ConfirmPasswordEntry_Focused()
        {
            PasswordLabel.Opacity = 0;
            ConfirmPasswordLabel.Opacity = 0;
            ConfirmPasswordLabel.FadeTo(1, 250);

        }
        void AllEntry_Unfocused()
        {
            OpacityZero();

        }
    }
}
