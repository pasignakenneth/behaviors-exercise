﻿using System;
using Xamarin.Forms;
namespace BehaviorsExercise.Utilities.Behaviors
{
    public class EntryBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.Unfocused += OnEntryUnfocused;
            entry.Focused += OnEntryFocused;
            base.OnAttachedTo(entry);

        }
        protected override void OnDetachingFrom(Entry entry)
        {
            entry.Unfocused -= OnEntryUnfocused;
            entry.Focused -= OnEntryFocused;
            base.OnDetachingFrom(entry);

        }
        void OnEntryUnfocused(object sender, EventArgs e)
        {
            var entry = (Entry)sender;

            if(entry.Text.Length==0)
            {
                entry.Placeholder = "Required";
                entry.PlaceholderColor = Color.Red;

            }
        }
        void OnEntryFocused(object sender, EventArgs e)
        {
            var entry = (Entry)sender;
            entry.Placeholder = "";

        }

    }
}
